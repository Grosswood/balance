﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Archivist : MonoBehaviour
{
	public GameObject[] ParentObject;

	private string _levelInfo;

	void Start()
	{
		foreach (GameObject go in ParentObject)
		{
			foreach (Transform child in go.transform)
			{
				string objectData = "";
				string extra = "0";

				if (child.GetComponent<Platform> ())
					objectData = "Platform";
				else if (child.GetComponent<Coin> ())
					objectData = "Coin";
				else if (child.GetComponent<Jumper> ())
				{
					objectData = "Jumper";
					extra = "" + child.GetComponent<Jumper> ().PushPower;
				}					
				else if (child.GetComponent<Edge> ())
					objectData = "Edge";
				else if (child.GetComponent<Sphere> ())
					objectData = "Sphere";
				else if (child.GetComponent<Exit> ())
					objectData = "Exit";
				

				if (objectData.Length > 0)
				{
					objectData +=
						" " + child.position.x +
						" " + child.position.y +
						" " + child.position.z +
						" " + child.eulerAngles.x +
						" " + child.eulerAngles.y +
						" " + child.eulerAngles.z +
						" " + child.localScale.x +
						" " + child.localScale.y +
						" " + child.localScale.z +
						" " + extra +
						"\n";
					_levelInfo += objectData;
				}
			}
		}
		Debug.Log (_levelInfo);
	}
}