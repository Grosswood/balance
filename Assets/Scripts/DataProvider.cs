﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Globalization;
using UnityEngine;
using UnityEngine.UI;

public class DataProvider : MonoBehaviour
{
	public GameObject PlayerGO;
	public TextAsset Data;
	public List<List<LevelObject>> DataForAllLevels = new List<List<LevelObject>> ();

	private static DataProvider _instance;

	public static DataProvider Instance
	{
		get
		{
			if (_instance != null) return _instance;
			_instance = FindObjectOfType<DataProvider>();
			_instance.Load();
			return _instance;
		}
	}

	private void Load()
	{
		var text = Data.text.Replace("\r\n", "\n");
		var textAllLevels = text.Split(new[] { "\n\n" }, StringSplitOptions.RemoveEmptyEntries);

		foreach (var l in textAllLevels)
		{
			var textLevel = new List<LevelObject> ();
			var objects = l.Split(new[] { "\n" }, StringSplitOptions.RemoveEmptyEntries);
			foreach (var o in objects)
				textLevel.Add (new LevelObject (o));
			DataForAllLevels.Add (textLevel);
		}
	}


	public void LoadLevel(int levelIndex)
	{
		for (int i = 0; i < transform.childCount; i++)
			Destroy (transform.GetChild(i).gameObject);
		
		foreach (var o in DataForAllLevels[levelIndex])
			o.Create (this.transform);
		
		PlayerGO.transform.position = Vector3.zero;
		PlayerGO.GetComponentInChildren<Rigidbody>().velocity = Vector3.zero;
		PlayerGO.GetComponentInChildren<Rigidbody>().angularVelocity = Vector3.zero;
	}
}

public class LevelObject
{
	public float X, Y, Z, RotateX, RotateY, RotateZ, ScaleX, ScaleY, ScaleZ, Extra;
	public string ObjectType;

	public LevelObject(string objectValues)
	{
		var values = objectValues.Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
		ObjectType = values [0];
		X = float.Parse(values [1], CultureInfo.InvariantCulture.NumberFormat);
		Y = float.Parse(values [2], CultureInfo.InvariantCulture.NumberFormat);
		Z = float.Parse(values [3], CultureInfo.InvariantCulture.NumberFormat);
		RotateX = float.Parse(values [4], CultureInfo.InvariantCulture.NumberFormat);
		RotateY = float.Parse(values [5], CultureInfo.InvariantCulture.NumberFormat);
		RotateZ = float.Parse(values [6], CultureInfo.InvariantCulture.NumberFormat);
		ScaleX = float.Parse(values [7], CultureInfo.InvariantCulture.NumberFormat);
		ScaleY = float.Parse(values [8], CultureInfo.InvariantCulture.NumberFormat);
		ScaleZ = float.Parse(values [9], CultureInfo.InvariantCulture.NumberFormat);
		Extra = float.Parse(values [10], CultureInfo.InvariantCulture.NumberFormat);
	}

	public void Create(Transform parent)
	{
		var go = GameObject.Instantiate (Resources.Load<GameObject> ("Prefabs/" + ObjectType));
		go.transform.SetParent(parent);
		go.transform.position = new Vector3 (X, Y, Z);
		go.transform.eulerAngles = new Vector3 (RotateX, RotateY, RotateZ);
		go.transform.localScale = new Vector3 (ScaleX, ScaleY, ScaleZ);

		if (ObjectType == "Jumper")
			go.GetComponentInChildren<Jumper> ().PushPower = Extra;
	}
}