﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{
	public float DecayRate = 0.005f;
	public bool Unstable = false;
	
	private bool _crumbling = false;
	private float _alphaValue = 1f;
	
	void FixedUpdate ()
	{
		if (!_crumbling)
			return;

		_alphaValue -= DecayRate;
		GetComponent<Renderer>().material.color = new Color(1f, 1f, 1f, _alphaValue);

		if (_alphaValue < 0.01f)
			gameObject.SetActive(false);
	}

	void OnCollisionEnter(Collision collision)
	{
		if (Unstable && collision.gameObject.tag == "Player")
			_crumbling = true;
	}
}