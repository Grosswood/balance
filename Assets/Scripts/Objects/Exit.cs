﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Exit : MonoBehaviour
{
	void Start()
	{
		Reveal (false);
		GetComponentInParent<Items> ().LevelExit = this;
	}

	public void Reveal(bool shouldI)
	{
		GetComponentInChildren<BoxCollider> ().enabled = shouldI;
		GetComponentInChildren<MeshRenderer> ().enabled = shouldI;
	}

	void Update ()
	{
		if (Input.GetKeyDown("escape"))
			MenuUI.LoadLevel (-1);
	}

	void OnCollisionEnter(Collision collision)
	{
		if (collision.gameObject.tag == "Player")
		{
			MenuUI.Instance.ShowLevelPanel (true);
			Time.timeScale = 0f;
		}
	}
}
