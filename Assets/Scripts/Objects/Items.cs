﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Items : MonoBehaviour
{
	public Exit LevelExit;

	public void CountCoins()
	{
		int count = GetComponentsInChildren<Coin> ().Length;
		if (count == 0)
			LevelExit.Reveal (true);
	}
}
