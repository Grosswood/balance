﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmoothFollow : MonoBehaviour
{    
	public Transform Target;

	private Animator _animator;

	void Start()
	{
		_animator = GetComponentInChildren<Animator> ();
	}

	void LateUpdate ()
	{
		RotateWithButtons ();

		transform.position = Target.transform.position;
	}

	void RotateWithButtons()
	{
		if (Input.GetKeyDown ("q"))
		{
			_animator.SetTrigger ("left");
			_animator.ResetTrigger ("right");
		}
		if (Input.GetKeyDown ("e"))
		{
			_animator.SetTrigger ("right");
			_animator.ResetTrigger ("left");
		}
	}
}