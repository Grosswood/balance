﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jumper : MonoBehaviour
{
	public float PushPower = 500f;

	void OnCollisionEnter(Collision collision)
	{
		if (collision.gameObject.tag == "Player")
		{
			Vector3 direction = transform.TransformDirection (Vector3.up);
			collision.gameObject.GetComponentInChildren<Rigidbody> ().AddForce (direction * PushPower);
			Audio.Play ("tap");
		}
	}
}
