﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
	public float Speed = 5f;
	public float JumpHeight = 200f;
	public GameObject CameraParent;

	private Rigidbody _rb;

	void Start ()
	{
		_rb = GetComponentInChildren<Rigidbody> ();
	}

	void FixedUpdate ()
	{
		// Getting input
		float moveHorizontal = Input.GetAxis ("Horizontal");
		float moveVertical = Input.GetAxis ("Vertical");
		Vector3 movement = new Vector3 (moveHorizontal, 0.0f, moveVertical);
		if (movement.magnitude > 1f)
			movement.Normalize ();

		// Camera offset
		float offset = CameraParent.transform.eulerAngles.y;
		movement = Quaternion.Euler (0f, offset, 0f) * movement;

		// Moving player
		_rb.AddForce (movement * Speed);
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Coin")
			other.gameObject.SetActive (false);
	}

	void OnCollisionStay(Collision collision)
	{
		if (collision.gameObject.tag == "Floor" && Input.GetKeyDown("space"))
		{
			_rb.AddForce (Vector3.up * JumpHeight);
		}
	}
}
