﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
	public float RotateSpeed = 5f;

	void FixedUpdate ()
	{
		transform.RotateAround (transform.position, Vector3.up, RotateSpeed);
	}

	void OnDisable()
	{
		var items = GetComponentInParent<Items> ();
		if (items)
			items.CountCoins ();
	}
}
