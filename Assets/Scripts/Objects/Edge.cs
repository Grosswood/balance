﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Edge : MonoBehaviour
{
	void Start()
	{
		GetComponent<MeshRenderer> ().enabled = false;
	}

	void OnCollisionEnter(Collision collision)
	{
		if (collision.gameObject.tag == "Player")
		{
			MenuUI.LoadLevel (-1);
		}
	}
}
