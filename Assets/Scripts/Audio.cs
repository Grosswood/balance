﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Audio : MonoBehaviour
{
	private static Audio _current;
	private AudioSource _musicAudioSource;
	private float _soundVolume;
	private float _musicVolume;

	protected static Audio Current
	{
		get
		{
			if (_current == null)
			{
				var go = new GameObject("audio");
				_current = go.AddComponent<Audio>();
				_current.Init();
			}
			return _current;
		}
	}

	private void Init()
	{
		var musicGameObject = new GameObject("music");
		musicGameObject.transform.SetParent(transform);
		_musicAudioSource = musicGameObject.AddComponent<AudioSource>();

		Current._soundVolume = PlayerPrefs.GetFloat("sound", 1f);
		Current._musicVolume = PlayerPrefs.GetFloat("music", 0.5f);
	}

	public static AudioSource Play(string audioName)
	{
		var audioClip = Resources.Load<AudioClip>("Sound/" + audioName);
		var go = new GameObject("audio");
		var audioSource = go.AddComponent<AudioSource>();
		audioSource.clip = audioClip;
		audioSource.volume = SoundVolume;
		audioSource.Play();
		go.transform.SetParent(Current.transform);
		Destroy(go, audioClip.length + 0.01f);
		return audioSource;
	}

	public static void PlayMusic(string audioName)
	{
		var audioClip = Resources.Load<AudioClip>("Sound/" + audioName);
		Current._musicAudioSource.clip = audioClip;
		Current._musicAudioSource.loop = true;
		Current._musicAudioSource.Play();
		Current._musicAudioSource.volume = MusicVolume;
	}

	public static float SoundVolume
	{
		get
		{
			return Current._soundVolume;
		}
		set
		{
			PlayerPrefs.SetFloat("sound", value);
			Current._soundVolume = value;
		}
	}

	public static float MusicVolume
	{
		get
		{
			return Current._musicVolume;
		}
		set
		{
			PlayerPrefs.SetFloat("music", value);
			Current._musicVolume = value;
			Current._musicAudioSource.volume = value;
		}
	}
}