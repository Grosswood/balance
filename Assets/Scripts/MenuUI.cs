﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuUI : MonoBehaviour
{
	private static MenuUI _instance;
	private static int _currentLevel;

	public GameObject MenuPanel;

	public static MenuUI Instance
	{
		get
		{
			if (_instance != null) return _instance;
			_instance = FindObjectOfType<MenuUI>();
			return _instance;
		}
	}

	public static void LoadLevel(int index)
	{
		// type negative number to reload current level without changing
		if (index >= 0)
			_currentLevel = index;

		DataProvider.Instance.LoadLevel (_currentLevel);
		MenuUI.Instance.ShowLevelPanel (false);
	}

	public void LoadLevelButton(int index)
	{
		LoadLevel (index);
	}

	public void ShowLevelPanel(bool shouldI)
	{
		MenuPanel.SetActive (shouldI);
		Time.timeScale = shouldI ? 0f : 1f;
	}

	void Awake()
	{
		MenuUI.Instance.ShowLevelPanel (true);

		// taking value of DataProvider.Instance.DataForAllLevels.Count triggers lazy init of DataProvider

		for (int i = 0; i < DataProvider.Instance.DataForAllLevels.Count; i++)
		{
			var levelB = GameObject.Instantiate (Resources.Load<GameObject> ("Prefabs/button_level_select"));
			var localInt = i;
			levelB.GetComponentInChildren<Button> ().onClick.AddListener (delegate
			{
					LoadLevelButton (localInt);
			});
			levelB.GetComponentInChildren<Text> ().text = "Level " + localInt;
			levelB.transform.SetParent(MenuPanel.transform);
		}
	}
}